\input texinfo.tex
@c guile-gdbm.texi
@c
@c Copyright (C) 2004, 2005, 2007, 2011, 2022 Thien-Thi Nguyen
@c Copyright (C) 2001, 2003 Martin Grabmüller
@c
@c Permission is granted to make and distribute verbatim copies of
@c this manual provided the copyright notice and this permission notice
@c are preserved on all copies.

@c %**start of header
@setfilename guile-gdbm.info
@documentencoding UTF-8
@settitle Guile-GDBM
@iftex
@afourpaper
@end iftex
@c %**end of header

@include version.texi

@dircategory Guile modules
@direntry
* Guile-GDBM: (guile-gdbm).     GNU dbm bindings for Guile.
@end direntry


@c --- title page starts here ---

@titlepage
@title Guile-GDBM
@subtitle GNU dbm bindings for Guile
@subtitle Version @value{VERSION}
@author Martin Grabmüller

@c  The following two commands
@c  start the copyright page.
@page
@vskip 0pt plus 1filll
Copyright @copyright{} 2004, 2005, 2007, 2011, 2022 Thien-Thi Nguyen@*
Copyright @copyright{} 2001, 2003 Martin Grabmüller

Permission is granted to make and distribute verbatim copies of
this manual provided the copyright notice and this permission notice
are preserved on all copies.
@end titlepage

@c --- title page ends here ---

@contents

@syncodeindex vr cp
@syncodeindex fn cp

@ifnottex
@node Top
@top The (database gdbm) Module

This file documents Guile-GDBM, which provides the @code{(database gdbm)}
module, allowing Guile scheme programmers access to the functionality of GNU
dbm.  It documents version @value{VERSION} of Guile-GDBM.
@end ifnottex

@menu
* Introduction::                What is this all about?
* Quick Start::                 Quick start tutorial for Guile-GDBM.
* Using Guile-GDBM::            How to use the GNU dbm bindings.
* Index::                       Procedure index.
@end menu

@node Introduction
@chapter Introduction

The compiled module @code{(database gdbm)} provides access to the
procedures of the GNU dbm library.  You should refer to the GNU dbm
manual also, which provides some details not documented here.

Guile-GDBM can be used to store string data into GNU dbm database files.
Data always consists of a key and an associated value, both of which must
be Scheme strings.  Currently, other datatypes than strings can not be
stored into GNU dbm files.  Of course, you can convert the data you want
to store to strings before passing them to the Guile-GDBM procedures.

You can open a GNU dbm file, store data into it, fetch and delete data from
it, and iterate over all data entries in it.  You can reorganize the file to
compact free space.  You can access multiple GNU dbm files simultaneously.

On the other hand, there is no support for the DBM/NDBM compatibility
functions provided by GNU dbm.

@node Quick Start
@chapter Quick Start

This chapter gives a quick overview of what can be done with Guile-GDBM.
For more details and the complete procedure documentation, see
@ref{Using Guile-GDBM}.

Before any of the Guile-GDBM procedures is available, the @code{(database
gdbm)} module must be loaded (@ref{Loading Guile-GDBM}).

@example
guile> (use-modules (database gdbm))
@end example

@noindent
The first thing we want to do with the newly loaded module is create a
new GNU dbm file (@ref{Opening}).

@example
guile> (define db (gdbm-open "foo.db" 'create))
@end example

@noindent
The symbol @code{create} tells Guile-GDBM to create a new GNU dbm file.
Next, we'll store a value into the database and retrieve it (@ref{Data
Manipulation}).  The keys under which the data is stored, as well as the
values, must be strings.

@example
guile> (gdbm-store! db "foo" "bar" 'insert)
guile> (define v (gdbm-fetch db "foo"))
guile> v
"bar"
@end example

@noindent
When we have stored a little more data,

@example
guile> (gdbm-store! db "fumble" "braz" 'insert)
@end example

@noindent
we can iterate over all the keys with the procedures
@code{gdbm-first-key} and @code{gdbm-next-key} (@ref{Querying the
database}).

@example
guile> (define k (gdbm-first-key db))
guile> k
"foo"
guile> (set! k (gdbm-next-key db k))
guile> k
"fumble"
@end example

@noindent
Finally, when we are done with the database file, we can close it
(@ref{Closing the file}).

@example
guile> (gdbm-close! db)
@end example

@include using.texi

@node Index
@unnumbered Index

@printindex cp

@bye

@c guile-gdbm.texi ends here
