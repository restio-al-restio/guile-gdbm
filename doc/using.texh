@c using.texh
@c
@c Copyright (C) 2004, 2007, 2011, 2013, 2022 Thien-Thi Nguyen
@c Copyright (C) 2001, 2003 Martin Grabmüller
@c
@c Permission is granted to make and distribute verbatim copies of
@c this manual provided the copyright notice and this permission notice
@c are preserved on all copies.

@c This file was extracted from guile-gdbm.texi,
@c w/ @deffn blocks replaced by @tsin directives.

@node Using Guile-GDBM
@chapter Using Guile-GDBM

Dealing with GNU dbm files consists of several actions.  Like with
normal files, you first have to open the file.  Then you can store data
into the file, fetch data from the file, enumerate the available keys in
the file, and finally, you can close the file again.  Occasionally,
you may also wish to maintain the database file by compacting it.

@menu
* Loading Guile-GDBM::          How to load the module.
* Opening::                     How to open GNU dbm files.
* Data Manipulation::           How to store and fetch data.
* Querying the database::       Query the contents of a database.
* Errors::                      When things are not completely OK.
* Inconsistent state recovery:: Restoring normalcy, if possible.
* Closing the file::            How to close the GNU dbm file after use.
* Syncing the file::            Making sure the data is on disk.
* Reorganization::              Compacting database files.
* Options::                     Setting options.
@end menu

@node Loading Guile-GDBM
@section Loading Guile-GDBM

When Guile-GDBM is properly installed, it can be loaded into a running
Guile by using the @code{(database gdbm)} module.

@example
$ guile
guile> (use-modules (database gdbm))
guile>
@end example

@noindent
When this step causes any errors, either Guile-GDBM is not properly
installed, or you don't have the GNU dbm library installed.

One possible reason is that Guile cannot find the shared object library that
implements the module.  If your version of Guile supports module catalogs,
make sure that one of the module catalogs in your @code{%load-path} contains
an entry for @code{(database gdbm)}.  Otherwise, make sure there exists a
directory @file{database} in your @code{%load-path} and this directory
contains either the @file{gdbm} shared object library and related support
files, or symbolic links to those files.

Now you can test whether the Guile-GDBM bindings are working by calling
the @code{gdbm-version} procedure.

@example
guile> (gdbm-version)
"This is GDBM version 1.7.3, as of May 19, 1994."
@end example

@noindent
The returned string depends on your installed version of GNU dbm, of
course.

@tsin i gdbm-version

@node Opening
@section Opening GNU dbm files

Before using any of the other procedures, you have to open a GNU dbm
file with the procedure @code{gdbm-open}.

@tsin i gdbm-open

When a new database file is created, it is created with a file mode of
0622.  The GNU dbm library also allows the user to specify which block
size to use when writing and reading data.  Guile-GDBM uses the default
block size, which depends on the file system the file is located on.

The object returned by @code{gdbm-open} is a database handle, which can
be used with the other Guile-GDBM procedures.  Database handles are of a
special data type which can be tested with the predicate @code{gdbm?}.

@tsin i gdbm?

@tsin i gdbm-fdesc

@tsin i gdbm-copy-meta

@node Data Manipulation
@section Data Manipulation

This section documents the procedures for manipulating the data stored
in a GNU dbm file.

If you want to store data into a GNU dbm file, you have to call the
procedure @code{gdbm-store!} with a database file handle, a key, a value
and a flag which specifies what to do if the key already exists in the
database.

@tsin i gdbm-store!

Data previously stored into a GNU dbm file can be retrieved with the
procedure @code{gdbm-fetch}.  The key specifies which value will be
fetched from the database.

@tsin i gdbm-fetch

Of course, it is also possible to get rid of data which is no longer
needed.  Use @code{gdbm-delete!} to do that.  Please note that the space
in the GNU dbm file which gets freed is not automatically compacted, you
have to call @code{gdbm-reorganize!} (@pxref{Reorganization}) to do
that.

@tsin i gdbm-delete!

@node Querying the database
@section Querying the database

Several procedures are provided to test the contents of a GNU dbm
file, or to iterate over all available keys.
The procedure @code{gdbm-exists?} lets you test whether or not a given key
exists in the file.

@tsin i gdbm-exists?

@tsin i gdbm-count

The following two procedures can be used to iterate over all the keys.

@tsin i gdbm-first-key

@tsin i gdbm-next-key

Use the following code if you want to list all available keys ---
@var{db} must be a valid database handle, of course (@pxref{Opening}).

@lisp
(let loop ((key (gdbm-first-key db)))
  (if key
      (begin
        (write-line key)
        (loop (gdbm-next-key db key)))))
@end lisp

@node Errors
@section Errors

While GNU dbm has @code{gdbm_errno} (an rvalue integer),
Guile-GDBM has instead @code{gdbm-errsym}.

@tsin i gdbm-errsym

The symbol is one of the following set (note that your installation
may have more or less, depending on the precise version of GDBM):

@example
no-error
malloc-error
block-size-error
file-open-error
file-write-error
file-seek-error
file-read-error
bad-magic-number
empty-database
cant-be-reader
cant-be-writer
reader-cant-delete
reader-cant-store
reader-cant-reorganize
unknown-error
item-not-found
reorganize-failed
cannot-replace
illegal-data
opt-already-set
opt-illegal
byte-swapped
bad-file-offset
bad-open-flags
file-stat-error
file-eof
no-dbname
err-file-owner
err-file-mode
need-recovery
backup-failed
dir-overflow
bad-bucket
bad-header
bad-avail
bad-hash-table
bad-dir-entry
file-close-error
file-sync-error
file-truncate-error
@end example

If the symbolic name is not not descriptive enough for you,
there is also @code{gdbm-strerror}:

@tsin i gdbm-strerror

There are also per-db procedures that deal with errors.

@tsin i gdbm-last-errsym

@tsin i gdbm-last-syserr

@tsin i gdbm-check-syserr

@tsin i gdbm-clear-error

@tsin i gdbm-db-strerror

@tsin i gdbm-needs-recovery?

@heading The ``happy to throw'' bit

@cindex ``happy to throw'' bit
Some Guile-GDBM procedures are documented to
``Throw an exception if an error occurs.''
This design decision seemed like a good idea at the time, but is
nowadays regretted by the Guile-GDBM maintainer.
In order to keep backward compatibility with the Guile-GDBM 1.x
releases, yet at the same time provide a way forward towards a cleaner
design (where the user, and not Guile-GDBM, is responsible for
throwing exceptions if desired), Guile-GDBM keeps track of the
@dfn{happy to throw} bit.

When this is set (the default), those documented exception throws
occur on error.
When it is clear, the affected procedures fail if an error occurs
(indicated by returning @code{#f}), and you can find more information
via @code{gdbm-errsym} et al.
To examine or change this bit, use procedure @code{happy-to-throw}
(which is deliberately named sans the usual ``gdbm-'' prefix to
emphasize its separation from the rest of the procedures):

@tsin i happy-to-throw

@node Inconsistent state recovery
@section Inconsistent state recovery

Should the database file enter an @dfn{inconsistent state}
(@pxref{Recovery,,,gdbm,The GNU database manager}) and Guile-GDBM procs
start throwing errors saying the database file ``needs recovery'',
you can use @code{gdbm-recover} to salvage the data and return
everything to normal working order.

@tsin i gdbm-recover

@node Closing the file
@section Closing the file

When you have finished manipulating a GNU dbm file, you should close it
with the procedure @code{gdbm-close!}.  If you do not close the file
manually, it will be closed as soon as the last reference to the handle
is gone (this is handled by Guile's garbage collector).
After closing, the handle is invalidated and must no longer be passed to
any of the Guile-GDBM procedures.

@tsin i gdbm-close!

@node Syncing the file
@section Syncing the file

As we have seen in section @ref{Closing the file}, the cached contents
of a GNU dbm file are written to disk when the file is closed (either
implicitly by the garbage collector or explicitly by @code{gdbm-close!}).
If you need to make sure that the cached data is written to a file, but
don't want to close it, you can use the following procedure.

@tsin i gdbm-sync!

@node Reorganization
@section Reorganization

For performance reasons, the GNU dbm library does normally not compact
the free space in a database file when data is deleted.  This compaction
can be triggered by calling @code{gdbm-reorganize!}.

@tsin i gdbm-reorganize!

@node Options
@section Options

GNU dbm provides several runtime options for open connections.
You can use @code{gdbm-setopt!} to set these from Scheme code,
and @code{gdbm-getopt} to obtain the current values.
Note that the set of options handled by these procedures overlap,
but do not match exactly (some are write-only, some are read-only).

@tsin i gdbm-setopt!

@tsin i gdbm-getopt

@c using.texh ends here
