#!/bin/sh
# Usage: sh [-x] autogen.sh

guile-baux-tool snuggle m4 build-aux/
guile-baux-tool snuggle h src/snuggle/
guile-baux-tool import \
                re-prefixed-site-dirs \
                sofix \
                uninstall-sofixed \
                tsin \
                c-tsar \
                c2x \
                gen-scheme-wrapper \
                pascal-pool \
                gbaux-do

actually ()
{
    gnulib-tool --copy-file $1 $2
    rm -f "$2"~
}

actually doc/INSTALL.UTF-8 INSTALL

autoreconf -v -f -i

: Now run configure, and remember to --enable-maintainer-mode

# autogen.sh ends here
