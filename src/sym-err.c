/* sym-err.c

   Copyright (C) 2022 Thien-Thi Nguyen

   Guile-GDBM is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Guile-GDBM is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Guile-GDBM; if not, see <https://www.gnu.org/licenses/>.
*/

#include <stdint.h>
#include <stdbool.h>
#include <libguile.h>
#include "gi.h"
#include "sym-err.h"

struct symset {
  const size_t      count;
  const char * const name;
  const uint8_t    *pool;
};

typedef struct {
  const long * const val;
  bool classic;
  bool offset;
  struct symset ss;
} kp_t;

typedef struct {
  SCM  *smob;
  kp_t *kp;
} kp_init_t;

#include "k/err.c"

static SCM ht;

void
init_symbolic_error_codes (void)
{
  kp_t *kp = &error_kp;
  const struct symset *ss = &kp->ss;
  const uint8_t *pool = ss->pool;
  size_t count = ss->count;
  size_t i;

  ht = PERMANENT (MAKE_HASH_TABLE (count));
  for (i = 0; i < count; i++)
    {
      /* A ridiculous variable solely to avoid a GCC "warning: signed
         and unsigned type in conditional expression [-Wsign-compare]".
         Maybe there's a better way?  */
      long value = i;
      uint8_t len = *pool++;
      SCM sym = GC_PROTECT (SYMBOLN ((char *) pool, len));

      scm_hashq_set_x (ht, sym, NUM_FASTINT (i));
      scm_hashq_set_x (ht, NUM_FASTINT (value), sym);
      GC_UNPROTECT (sym);
      pool += len;
    }
}

static inline SCM
lookup (SCM key)
{
  return scm_hashq_ref (ht, key, SCM_BOOL_F);
}

long
enum2long (SCM symbol)
{
  return C_LONG (lookup (symbol));
}

SCM
long2enum (long num)
{
  return lookup (NUM_LONG (num));
}

/* sym-err.c ends here */
