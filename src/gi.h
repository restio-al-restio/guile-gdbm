/* gi.h

   Copyright (C) 2004, 2005, 2006, 2008, 2009,
     2011, 2012, 2013, 2022 Thien-Thi Nguyen

   Guile-GDBM is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Guile-GDBM is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Guile-GDBM; if not, see <https://www.gnu.org/licenses/>.
*/

#ifndef _GI_H_
#define _GI_H_

#include "snuggle/level.h"
#include "snuggle/humdrum.h"
#include "snuggle/finangle.h"
#include "snuggle/defsmob.h"
#include "snuggle/modsup.h"
#include "snuggle/mkhash.h"
#include "snuggle/fastint.h"

#define GIVENP(x)          (! SCM_UNBNDP (x))
#define NOT_FALSEP(x)      (SCM_NFALSEP (x))

#define RETURN_FALSE()                        return SCM_BOOL_F
#define RETURN_UNSPECIFIED()                  return SCM_UNSPECIFIED

#define ASSERT(what,expr,msg)  SCM_ASSERT ((expr), what, msg, FUNC_NAME)

#define SMOBDATA(obj)  ((void *) SCM_SMOB_DATA (obj))

#define PCHAIN(...)  (LISTIFY (__VA_ARGS__, SCM_UNDEFINED))

#define ERROR(blurb, ...)  SCM_MISC_ERROR (blurb, PCHAIN (__VA_ARGS__))

#define PRIMPROC             GH_DEFPROC

#define PERMANENT  scm_permanent_object

#endif /* _GI_H_ */

/* gi.h ends here */
