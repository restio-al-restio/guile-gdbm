/* gdbm.c --- GNU dbm bindings for Guile

   Copyright (C) 2003, 2004, 2007, 2011, 2012, 2013, 2021, 2022 Thien-Thi Nguyen
   Copyright (C) 2001 Martin Grabmüller <mgrabmue@cs.tu-berlin.de>

   Guile-GDBM is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Guile-GDBM is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Guile-GDBM; if not, see <https://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <gdbm.h>
#include "mode-flags.h"
#include "run-opts.h"
#include <libguile.h>
#include "gi.h"
#include "sym-err.h"


typedef union {
  char const * const str;
  SCM                sym;
} as_t;

/* Conventionally, the ‘as_t’ object is named ‘as’.  */
#define SYMBOLIC(x)  (x .as .sym)

struct runtime_opt {
  as_t as;
  int  r, w;
  enum { boolean, integer, numeric, string, symlist } type;
};

#define OSPEC_ENT(STRING,KNICK,TYPE)            \
  {                                             \
    .as   = { .str = #STRING },                 \
    .r    = GG_GET_ ## KNICK,                   \
    .w    = GG_SET_ ## KNICK,                   \
    .type = TYPE                                \
  }

static struct runtime_opt
runtime_options[] =
  {
    /* NB: Ordered greenhorn to venerable.  */
    OSPEC_ENT (blocksize,    BLOCKSIZE,    integer),
    OSPEC_ENT (dbname,       DBNAME,       string),
    OSPEC_ENT (mmap,         MMAP,         boolean),
    OSPEC_ENT (maxmapsize,   MAXMAPSIZE,   numeric),
    OSPEC_ENT (coalesceblks, COALESCEBLKS, boolean),
    OSPEC_ENT (centfree,     CENTFREE,     boolean),
    OSPEC_ENT (syncmode,     SYNCMODE,     boolean),
    OSPEC_ENT (flags,        FLAGS,        symlist),
    OSPEC_ENT (fastmode,     FASTMODE,     boolean),
    OSPEC_ENT (cachesize,    CACHESIZE,    numeric)
  };

static const size_t n_runtime_options =
  sizeof (runtime_options) / sizeof (struct runtime_opt);

#undef OSPEC_ENT


/* Smob type code for GNU DBM database files.  */
static long gg_db_tag = 0;

/* Smob guts.  */
#define DBF(smob)  SMOBDATA (smob)
#define DBFP(obj)  SCM_SMOB_PREDICATE (gg_db_tag, obj)

/* Smob print hook for GDBM files.  */
static
int
gdbmfile_print (SCM gdbmfile, SCM port, scm_print_state *pstate)
{
  GDBM_FILE dbf = DBF (gdbmfile);
  char beg[] = "#<gdbm-file ";
  char end[] = ">";
  char buf[42];

  if (!dbf)
    snprintf (buf, 42, "%s(closed)%s", beg, end);
  else
    snprintf (buf, 42, "%s%lx%s", beg, (unsigned long) dbf, end);
  scm_puts (buf, port);
  return 1;
}

/* Free function for GDBM smobs.  Closes the GDBM file if that was not
   already done explicitly via @code{gdbm-close!}.  */
static
size_t
gdbmfile_free (SCM gdbmfile)
{
  GDBM_FILE dbf = DBF (gdbmfile);

  if (dbf)
    gdbm_close (dbf);
  return 0;
}


#define SIMPLE_SYMBOL(frag) \
  SCM_SYMBOL (gg_sym_ ## frag, # frag)

/* Symbols to be passed to @code{gdbm-open} to specify the open mode.  */
SIMPLE_SYMBOL (read);
SIMPLE_SYMBOL (write);
SIMPLE_SYMBOL (create);
SIMPLE_SYMBOL (new);

struct mode_flag {
  as_t as;
  int  val;
};

#define MODE_FLAG(STRING,KNICK)                 \
  {                                             \
    .as  = { .str = #STRING },                  \
    .val = GG_ ## KNICK                         \
  }

static struct mode_flag mode_flags[] =
  {
    /* NB: Ordered greenhorn to venerable.  */
    MODE_FLAG (cloerror,CLOERROR),
    MODE_FLAG (bsexact, BSEXACT),
    MODE_FLAG (cloexec, CLOEXEC),
    MODE_FLAG (nommap,  NOMMAP),
    MODE_FLAG (nolock,  NOLOCK),
    MODE_FLAG (sync,    SYNC)
  };

#undef MODE_FLAG

static const size_t n_mode_flags =
  sizeof (mode_flags) / sizeof (struct mode_flag);

/* These symbols are passed to @code{gdbm-store!}.  */
SIMPLE_SYMBOL (insert);
SIMPLE_SYMBOL (replace);

#undef SIMPLE_SYMBOL
#define SYM(frag)  (gg_sym_ ## frag)


/* Supporting abstractions.  */

static int happy_to_throw;

static
void
file_not_open_error (const char *FUNC_NAME, SCM db)
{
  ERROR ("gdbm file not open: ~A", db);
}

#define SETCHK_DBF()                            \
  do {                                          \
    dbf = DBF (db);                             \
    if (!dbf)                                   \
      file_not_open_error (FUNC_NAME, db);      \
  } while (0)

static
void
signal_error_with_message (const char *FUNC_NAME)
{
  const char * msg = gdbm_strerror (gdbm_errno);

  ERROR ("gdbm error: ~A", STRING (msg));
}

#define THROW_ERROR_IF(condition) do            \
    {                                           \
      if (condition)                            \
        signal_error_with_message (FUNC_NAME);  \
    }                                           \
  while (0)

#define THROW_ERROR_IF_NEGATIVE(var)  THROW_ERROR_IF (0 > var)

#define RETURN_FALSE_IF_NEGATIVE(var)  do       \
    {                                           \
      if (0 > var)                              \
        return SCM_BOOL_F;                      \
    }                                           \
  while (0)

#define SETDATUMPTRSIZE(the_datum,the_string)  do       \
    {                                                   \
      FINANGLE_RAW (the_string);                        \
      the_datum.dptr = RS (the_string);                 \
      the_datum.dsize = RLEN (the_string);              \
    }                                                   \
  while (0)

static
void
signal_invalid (const char *FUNC_NAME, const char *kind, SCM what)
{
  ERROR ("Invalid ~A: ~A", STRING (kind), what);
}

#define INVALID(kind,what)  signal_invalid (FUNC_NAME, kind, what)

#define CHKDB()  ASSERT (db, DBFP (db), 1)

typedef struct setopt_details {
  const char               *who;
  const int                 dir;
  const struct runtime_opt *opt;
  GDBM_FILE                *dbf;
} sdet_t;

static void
recognize_option (sdet_t *sdet, SCM obj)
{
  const char *FUNC_NAME = sdet->who;
  size_t i;

  SCM_VALIDATE_SYMBOL (2, obj);
  for (i = 0; i < n_runtime_options; i++)
    if (EQ (obj, SYMBOLIC (runtime_options[i])))
      {
        if (SORRY != (GDBM_READER == sdet->dir
                      ? runtime_options[i].r
                      : runtime_options[i].w))
          {
            sdet->opt = &runtime_options[i];
            return;
          }
        break;
      }
  INVALID ("option", obj);
}

static int
do_setopt (const sdet_t *sdet, void *var, size_t sz)
{
  const char *FUNC_NAME = sdet->who;
  int ret = gdbm_setopt (*(sdet->dbf), (GDBM_READER == sdet->dir
                                        ? sdet->opt->r
                                        : sdet->opt->w),
                         var, sz);

  if (happy_to_throw)
    THROW_ERROR_IF_NEGATIVE (ret);

  return ret;
}


/* The gdbm-* functions.  */

static int
recognize_mode (const char *FUNC_NAME, SCM mode)
{
  int md;

  SCM_VALIDATE_SYMBOL (2, mode);
  if (EQ (mode, SYM (read)))
    md = GDBM_READER;
  else if (EQ (mode, SYM (write)))
    md = GDBM_WRITER;
  else if (EQ (mode, SYM (create)))
    md = GDBM_WRCREAT;
  else if (EQ (mode, SYM (new)))
    md = GDBM_NEWDB;
  else
    INVALID ("mode", mode);
  return md;
}

#define RECOGNIZE_MODE(obj)  recognize_mode (FUNC_NAME, obj)

static int
compute_mode (const char *FUNC_NAME, SCM mode)
{
  int md = GDBM_READER;

  if (GIVENP (mode))
    {
      if (PAIRP (mode))
        {
          SCM flags;

          md = RECOGNIZE_MODE (CAR (mode));
          for (flags = CDR (mode);
               PAIRP (flags);
               flags = CDR (flags))
            {
              SCM flag = CAR (flags);
              int i;

              SCM_VALIDATE_SYMBOL (2, flag);
              for (i = 0; i < n_mode_flags; i++)
                if (EQ (flag, SYMBOLIC (mode_flags[i])))
                  break;
              if (n_mode_flags == i)
                INVALID ("mode flag", flag);
              md |= mode_flags[i].val;
            }
        }
      else
        md = RECOGNIZE_MODE (mode);
    }

  return md;
}

#define COMPUTE_MODE(obj)  compute_mode (FUNC_NAME, obj)

PRIMPROC
(gg_open, "gdbm-open", 1, 1, 0,
 (SCM file, SCM mode),
 doc: /***********
Open the GNU dbm file named @var{file} in mode @var{mode} (a symbol),
and return a handle which can be used in the other Guile-GDBM procedures.
Valid values of @var{mode} are:

@table @code
@item read
Request read-access to the database; any call to
@code{gdbm-store!} or @code{gdbm-delete!} will fail.  Many
readers can access the database at the same time.

@item write
Request both read- and write-access to the database.
Furthermore access is exclusive.

@item create
Like @code{write}, but additionally
if the database does not exist, create a new one.

@item new
Request creation of a new database, regardless of whether
one existed previously, with both read- and write-access.
Any file with the same name will be overwritten
when this mode is specified.
@end table

@var{mode} may also be a list of the form
@code{(@var{mode} [@var{flag}@dots{}])},
where @var{flag}@dots{} are symbols from the set:

@table @code
@item sync
Cause all database operations to be synchronized to the disk.

@item lock
Signal that any locking on @var{file} will be done by
the application; GNU dbm will not perform locking, itself.

@item nommap
Disable the memory mapping mechanism.

@item cloexec
Enable the close-on-exec flag for the database file descriptor.
Note that this flag is valid only if the @code{open}
system call supports the @code{O_CLOEXEC} flag.

@item bsexact
Cause @code{gdbm-open} to fail if the requested block size
cannot be used without adjustment.  Since Guile-GDBM does
not provide a way to request a block size other than the
``system block size'', this flag is not currently useful.
If you really need to specify block size, file a bug report.
@end table

If @var{mode} is omitted, it defaults to @code{read}.

Throw an exception if an error occurs.  */)
{
#define FUNC_NAME s_gg_open
  range_t cfile;
  GDBM_FILE dbf;
  int md;

  SCM_VALIDATE_STRING (1, file);

  md = COMPUTE_MODE (mode);
  FINANGLE (file);
  dbf = gdbm_open (RS (file), 0, md, 0622, NULL);
  UNFINANGLE (file);

  if (happy_to_throw)
    THROW_ERROR_IF (! dbf);

  if (! dbf)
    RETURN_FALSE ();

  SCM_RETURN_NEWSMOB (gg_db_tag, dbf);
#undef FUNC_NAME
}

PRIMPROC
(gg_close, "gdbm-close!", 1, 0, 0,
 (SCM db),
 doc: /***********
Close @var{db}.  Write all not yet committed data to disk.

For GDBM versions where the underlying C function @code{gdbm_close}
has a return value, return @code{#t} on success.  */)
{
#define FUNC_NAME s_gg_close
  GDBM_FILE dbf;

  CHKDB ();
  SETCHK_DBF ();
  SCM_SET_SMOB_DATA (db, NULL);
#if GDBM_CLOSE_HAS_RV

  int ret = gdbm_close (dbf);

  RETURN_FALSE_IF_NEGATIVE (ret);
  return SCM_BOOL_T;

#else  /* !GDBM_CLOSE_HAS_RV */
  gdbm_close (dbf);
  RETURN_UNSPECIFIED ();
#endif  /* !GDBM_CLOSE_HAS_RV */
#undef FUNC_NAME
}

PRIMPROC
(gg_p, "gdbm?", 1, 0, 0,
 (SCM obj),
 doc: /***********
Return @code{#t} if @var{obj} is a GNU dbm file handle,
@code{#f} otherwise.  */)
{
#define FUNC_NAME s_gg_p
  return BOOLEAN (DBFP (obj));
#undef FUNC_NAME
}

PRIMPROC
(gg_version, "gdbm-version", 0, 1, 0,
 (SCM config),
 doc: /***********
Return a string containing the version of the GNU dbm library
currently in use.  Optional arg @var{config} non-@code{#f}
means to return instead a list of selected @code{HAVE_foo}
symbols determined at build time by the configure script.  */)
{
#define FUNC_NAME s_gg_version
  return (! (GIVENP (config) && NOT_FALSEP (config))
          ? STRING (gdbm_version)
          : LISTIFY (
#ifdef HAVE_GDBM_SETOPT
                     SYMBOL ("HAVE_GDBM_SETOPT"),
#endif
#ifdef HAVE_GDBM_FDESC
                     SYMBOL ("HAVE_GDBM_FDESC"),
#endif
#ifdef HAVE_GDBM_COUNT
                     SYMBOL ("HAVE_GDBM_COUNT"),
#endif
#ifdef HAVE_GDBM_RECOVER
                     SYMBOL ("HAVE_GDBM_RECOVER"),
#endif
#ifdef GDBM_SYNC_HAS_RV
                     SYMBOL ("GDBM_SYNC_HAS_RV"),
#endif
#ifdef GDBM_CLOSE_HAS_RV
                     SYMBOL ("GDBM_CLOSE_HAS_RV"),
#endif
                     SCM_UNDEFINED));
#undef FUNC_NAME
}

PRIMPROC
(gg_store_x, "gdbm-store!", 4, 0, 0,
 (SCM db, SCM key, SCM value, SCM flag),
 doc: /***********
Store @var{value} under @var{key} (both strings) in @var{db}.
@var{flag} is a symbol which
indicates what to do if @var{key} already exists:
@code{insert} means do nothing;
@code{replace} means overwrite the old value.
Throw an exception if an error occurs.
Return @code{#t} on success.  */)
{
#define FUNC_NAME s_gg_store_x
  range_t ckey, cvalue;
  GDBM_FILE dbf;
  int ret, md;
  datum k, v;

  CHKDB ();
  SCM_VALIDATE_STRING (2, key);
  SCM_VALIDATE_STRING (3, value);
  SETCHK_DBF ();
  SCM_VALIDATE_SYMBOL (1, flag);
  if (EQ (flag, SYM (replace)))
    md = GDBM_REPLACE;
  else if (EQ (flag, SYM (insert)))
    md = GDBM_INSERT;
  else
    INVALID ("flag", flag);
  SETDATUMPTRSIZE (k, key);
  SETDATUMPTRSIZE (v, value);
  ret = gdbm_store (dbf, k, v, md);
  UNFINANGLE (key);
  UNFINANGLE (value);

  if (happy_to_throw)
    THROW_ERROR_IF_NEGATIVE (ret);

  RETURN_FALSE_IF_NEGATIVE (ret);
  return SCM_BOOL_T;
#undef FUNC_NAME
}

static
SCM
data_or_false (datum *d)
{
  if (d->dptr)
    {
      SCM res = BSTRING (d->dptr, d->dsize);

      free (d->dptr);
      return res;
    }
  RETURN_FALSE ();
}

PRIMPROC
(gg_fetch, "gdbm-fetch", 2, 0, 0,
 (SCM db, SCM key),
 doc: /***********
If @var{db} contains @var{key}, return its associated value.
Otherwise, return @code{#f}.
Throw an exception if an error occurs.  */)
{
#define FUNC_NAME s_gg_fetch
  range_t ckey;
  GDBM_FILE dbf;
  datum k, v;

  CHKDB ();
  SCM_VALIDATE_STRING (2, key);
  SETCHK_DBF ();
  SETDATUMPTRSIZE (k, key);
  v = gdbm_fetch (dbf, k);
  UNFINANGLE (key);
  return data_or_false (&v);
#undef FUNC_NAME
}

PRIMPROC
(gg_exists_p, "gdbm-exists?", 2, 0, 0,
 (SCM db, SCM key),
 doc: /***********
Return @code{#t} if @var{db} contains @var{key}.
Otherwise, return @code{#f}.  */)
{
#define FUNC_NAME s_gg_exists_p
  range_t ckey;
  GDBM_FILE dbf;
  datum k;
  int rv;

  CHKDB ();
  SCM_VALIDATE_STRING (2, key);
  SETCHK_DBF ();
  SETDATUMPTRSIZE (k, key);
  rv = gdbm_exists (dbf, k);
  UNFINANGLE (key);
  return BOOLEAN (rv);
#undef FUNC_NAME
}

PRIMPROC
(gg_delete_x, "gdbm-delete!", 2, 0, 0,
 (SCM db, SCM key),
 doc: /***********
Delete @var{key} and its associated value from @var{db}.
Throw an exception if an error occurs.
Return @code{#t} on success.  */)
{
#define FUNC_NAME s_gg_delete_x
  range_t ckey;
  GDBM_FILE dbf;
  datum k;
  int ret;

  CHKDB ();
  SCM_VALIDATE_STRING (2, key);
  SETCHK_DBF ();
  SETDATUMPTRSIZE (k, key);
  ret = gdbm_delete (dbf, k);
  UNFINANGLE (key);

  if (happy_to_throw)
    THROW_ERROR_IF_NEGATIVE (ret);

  RETURN_FALSE_IF_NEGATIVE (ret);
  return SCM_BOOL_T;
#undef FUNC_NAME
}

PRIMPROC
(gg_first_key, "gdbm-first-key", 1, 0, 0,
 (SCM db),
 doc: /***********
Return the first existing key in @var{db},
or @code{#f} if no keys exist.  */)
{
#define FUNC_NAME s_gg_first_key
  GDBM_FILE dbf;
  datum k;

  CHKDB ();
  SETCHK_DBF ();
  k = gdbm_firstkey (dbf);
  return data_or_false (&k);
#undef FUNC_NAME
}

PRIMPROC
(gg_next_key, "gdbm-next-key", 2, 0, 0,
 (SCM db, SCM key),
 doc: /***********
Return the key following @var{key} in @var{db},
or @code{#f} if no more keys exist.  */)
{
#define FUNC_NAME s_gg_next_key
  range_t ckey;
  GDBM_FILE dbf;
  datum k, n;

  CHKDB ();
  SCM_VALIDATE_STRING (2, key);
  SETCHK_DBF ();
  SETDATUMPTRSIZE (k, key);
  n = gdbm_nextkey (dbf, k);
  UNFINANGLE (key);
  return data_or_false (&n);
#undef FUNC_NAME
}

PRIMPROC
(gg_reorganize_x, "gdbm-reorganize!", 1, 0, 0,
 (SCM db),
 doc: /***********
Reorganize @var{db} so that free space is compacted.
Throw an exception if an error occurs.
Return @code{#t} on success.  */)
{
#define FUNC_NAME s_gg_reorganize_x
  GDBM_FILE dbf;
  int ret;

  CHKDB ();
  SETCHK_DBF ();
  ret = gdbm_reorganize (dbf);

  if (happy_to_throw)
    THROW_ERROR_IF_NEGATIVE (ret);

  RETURN_FALSE_IF_NEGATIVE (ret);
  return SCM_BOOL_T;
#undef FUNC_NAME
}

PRIMPROC
(gg_sync_x, "gdbm-sync!", 1, 0, 0,
 (SCM db),
 doc: /***********
Flush any buffered output for @var{db} to disk.
This is normally performed automatically as part of the
@code{gdbm-close!} operation, so this procedure is almost
always unnecessary.

For GDBM versions where the underlying C function @code{gdbm_sync}
has a return value, return @code{#t} on success.  */)
{
#define FUNC_NAME s_gg_sync_x
  GDBM_FILE dbf;

  CHKDB ();
  SETCHK_DBF ();
#if GDBM_SYNC_HAS_RV

  int ret = gdbm_sync (dbf);

  RETURN_FALSE_IF_NEGATIVE (ret);
  return SCM_BOOL_T;

#else  /* !GDBM_SYNC_HAS_RV */
  gdbm_sync (dbf);
  RETURN_UNSPECIFIED ();
#endif  /* !GDBM_SYNC_HAS_RV */
#undef FUNC_NAME
}

PRIMPROC
(gg_setopt_x, "gdbm-setopt!", 3, 0, 0,
 (SCM db, SCM opt, SCM val),
 doc: /***********
For connection @var{db} set option @var{opt} to @var{val}.
@var{opt} is a symbol; @var{val} depends on @var{opt}.
For boolean options, a non-@code{#f} value enables, else disables.

@table @asis
@item @code{cachesize} (integer)
Set the size of the internal bucket cache.
Note that attempting to set the cache size after any db
accesses occur will result in an error.

@item @code{fastmode} (boolean)
@strong{OBSOLETE!}  The logical inverse of @code{syncmode}.

@item @code{syncmode} (boolean)
Enable/disable per-operation synchronization to disk.

@item @code{centfree} (boolean)
Enable/disable central free block pool.

@item @code{coalesceblks} (boolean)
Enable/disable merging of adjacent free blocks.

@item @code{mmap} (boolean)
Enable/disable memory mapping.

@item @code{maxmapsize} (integer)
The maximum size of a memory mapped region.
@end table

Throw an exception if an error occurs.
Return @code{#t} on success.

For versions of GDBM that do not support setting options,
do nothing and return @code{#f}.  */)
{
#define FUNC_NAME s_gg_setopt_x
  GDBM_FILE dbf;
#ifdef HAVE_GDBM_SETOPT
  sdet_t sdet =
    {
      .who = FUNC_NAME,
      .dir = GDBM_WRITER,
      .dbf = &dbf
    };
#endif

  CHKDB ();
  SETCHK_DBF ();
#ifndef HAVE_GDBM_SETOPT
  RETURN_FALSE ();
#else /* HAVE_GDBM_SETOPT */

  int ret;

  recognize_option (&sdet, opt);
  switch (sdet.opt->type)
    {
#define JAM(var)  ret = do_setopt (&sdet, &var, sizeof (var))

    case numeric:
      {
        size_t numeric;

        SCM_VALIDATE_UINT_COPY (3, val, numeric);
        JAM (numeric);
      }
      break;

    case boolean:
      {
        int boolean = C_BOOL (val);

        JAM (boolean);
      }
      break;

    case integer:
    case symlist:
    case string:
      /* Should never get here.  */
      abort ();
#undef JAM
    }

  RETURN_FALSE_IF_NEGATIVE (ret);
  return SCM_BOOL_T;
#endif /* HAVE_GDBM_SETOPT */
#undef FUNC_NAME
}

PRIMPROC
(gg_getopt, "gdbm-getopt", 2, 0, 0,
 (SCM db, SCM opt),
 doc: /***********
For connection @var{db}, return the status of option @var{opt},
if available.  Do nothing and return @code{#f} otherwise.

@table @asis
@item @code{cachesize} (integer)
The size of the internal bucket cache.

@item @code{flags} (list of symbols)
The flags describing the state of the database, exactly one of:
@code{read}, @code{write}, @code{create}, @code{new},
followed by zero or more of: @code{sync}, @code{nolock},
@code{nommap}, @code{cloexec}.

@item @code{syncmode} (boolean)
Whether or not synchronization is enabled.

@item @code{centfree} (boolean)
Whether or not there is a central free block pool.

@item @code{coalesceblks} (boolean)
Whether or not adjacent free blocks are merged.

@item @code{maxmapsize} (integer)
The maximum size of a memory mapped region.

@item @code{mmap} (boolean)
Whether or not memory mapping is enabled.

@item @code{dbname} (string)
The name of the database disk file.

@item @code{blocksize} (integer)
The block size used for database file transfers.
@end table

Throw an exception if an error occurs.  */)
{
#define FUNC_NAME s_gg_getopt
  GDBM_FILE dbf;
  SCM rv = SCM_BOOL_F;
#ifdef HAVE_GDBM_SETOPT
  sdet_t sdet =
    {
      .who = FUNC_NAME,
      .dir = GDBM_READER,
      .dbf = &dbf
    };
#endif

  CHKDB ();
  SETCHK_DBF ();

#ifdef HAVE_GDBM_SETOPT

  recognize_option (&sdet, opt);

  switch (sdet.opt->type)
    {
#define RECEIVE(var)  do_setopt (&sdet, &var, sizeof (var))

    case integer:
      {
        int integer;

        RECEIVE (integer);
        rv = NUM_INT (integer);
      }
      break;

    case numeric:
      {
        size_t numeric;

        RECEIVE (numeric);
        rv = NUM_ULONG (numeric);
      }
      break;

    case boolean:
      {
        int boolean;

        RECEIVE (boolean);
        rv = BOOLEAN (boolean);
      }
      break;

    case symlist:
      {
        /* We used to use ‘size_t’ here, but that results in an
           "illegal operation" runtime error on a host where
           ‘sizeof (size_t)’ is 8, while ‘sizeof (int)’ is 4.  */
        int flags;
        int i;

        RECEIVE (flags);
        rv = SCM_EOL;

#define PUSH(x)  rv = CONS (x, rv)

        for (i = 0; i < n_mode_flags; i++)
          {
            int mask = mode_flags[i].val;

            if (! (0 > mask)
                && mask == (mask & flags))
              {
                PUSH (SYMBOLIC (mode_flags[i]));
                flags &= ~mask;
              }
          }

#define EXACTLY(knick,sym)  case GDBM_ ## knick: PUSH (SYM (sym)); break

        switch (flags)
          {
            EXACTLY (NEWDB,   new);
            EXACTLY (WRCREAT, create);
            EXACTLY (WRITER,  write);
            EXACTLY (READER,  read);
          }

#undef EXACTLY
#undef PUSH
      }
      break;

    case string:
      {
        char *string;

        RECEIVE (string);
        rv = STRING (string);
        free (string);
      }
      break;

#undef RECEIVE
    }
#endif  /* HAVE_GDBM_SETOPT */

  return rv;
#undef FUNC_NAME
}

PRIMPROC
(gg_errsym, "gdbm-errsym", 0, 0, 0,
 (void),
 doc: /***********
Return the symbolic value of underlying C @code{gdbm_errno},
or @code{#f} if the current value is invalid.  */)
{
  return long2enum (gdbm_errno);
}

PRIMPROC
(gg_strerror, "gdbm-strerror", 1, 0, 0,
 (SCM errsym),
 doc: /***********
Return a string describing error symbol @var{errsym},
or @code{#f} if @var{errsym} is not recognized.  */)
{
#define FUNC_NAME s_gg_strerror
  long num;
  const char *msg;

  SCM_VALIDATE_SYMBOL (1, errsym);

  num = enum2long (errsym);
  RETURN_FALSE_IF_NEGATIVE (num);

  msg = gdbm_strerror (num);
  if (! msg)
    RETURN_FALSE ();

  return STRING (msg);
#undef FUNC_NAME
}

PRIMPROC
(gg_last_errsym, "gdbm-last-errsym", 1, 0, 0,
 (SCM db),
 doc: /***********
Return the symbolic code of the most recent error encountered when
operating on @var{db}, or @code{#f} for GDBM versions that do
not provide the underlying C function @code{gdbm_last_errno}.  */)
{
#define FUNC_NAME s_gg_last_errsym
  GDBM_FILE dbf;

  CHKDB ();
  SETCHK_DBF ();

#if HAVE_GDBM_RECOVER

  return long2enum (gdbm_last_errno (dbf));

#else  /* !HAVE_GDBM_RECOVER */
  RETURN_FALSE ();
#endif /* !HAVE_GDBM_RECOVER */
#undef FUNC_NAME
}

PRIMPROC
(gg_last_syserr, "gdbm-last-syserr", 1, 0, 0,
 (SCM db),
 doc: /***********
Return the last @code{errno} (integer) associated with the most
recent error encountered when operating on @var{db}, or 0 (zero)
if there is no association, or @code{#f} for GDBM versions that
do not provide the underlying C function @code{gdbm_last_syserr}.

Currently, these are the errors that have an associated @code{errno}:

@example
file-open-error
file-write-error
file-seek-error
file-read-error
file-stat-error
backup-failed
file-close-error
file-sync-error
file-truncate-error
err-snapshot-clone
err-realpath
err-usage
@end example  */)
{
#define FUNC_NAME s_gg_last_syserr
  GDBM_FILE dbf;

  CHKDB ();
  SETCHK_DBF ();

#if HAVE_GDBM_RECOVER

  return NUM_INT (gdbm_last_syserr (dbf));

#else  /* !HAVE_GDBM_RECOVER */
  RETURN_FALSE ();
#endif /* !HAVE_GDBM_RECOVER */
#undef FUNC_NAME
}

PRIMPROC
(gg_check_syserr, "gdbm-check-syserr", 1, 0, 0,
 (SCM err),
 doc: /***********
Return @code{#t} if system @code{errno} value should be checked
to get more info on the error described by symbol @var{err}.

Do nothing and return @code{#f} for versions of GDBM that do not
provide the underlying C function @code{gdbm_check_syserr}.  */)
{
#define FUNC_NAME s_gg_check_syserr
  SCM_VALIDATE_SYMBOL (1, err);

#if HAVE_GDBM_RECOVER

  return BOOLEAN (gdbm_check_syserr (enum2long (err)));

#else  /* !HAVE_GDBM_RECOVER */
  RETURN_FALSE ();
#endif  /* !HAVE_GDBM_RECOVER */
#undef FUNC_NAME
}

PRIMPROC
(gg_clear_error, "gdbm-clear-error", 1, 0, 0,
 (SCM db),
 doc: /***********
Clear the error state for @var{db}.
Do nothing for versions of GDBM that do not provide the
underlying C function @code{gdbm_clear_error}.  */)
{
#define FUNC_NAME s_gg_clear_error
  GDBM_FILE dbf;

  CHKDB ();
  SETCHK_DBF ();
#if HAVE_GDBM_RECOVER

  gdbm_clear_error (dbf);

#else  /* !HAVE_GDBM_RECOVER */
  RETURN_UNSPECIFIED ();
#endif /* !HAVE_GDBM_RECOVER */
#undef FUNC_NAME
}

PRIMPROC
(gg_db_strerror, "gdbm-db-strerror", 1, 0, 0,
 (SCM db),
 doc: /***********
Like @code{gdbm-strerror}, except specific to @var{db}.
This can sometimes give more information than @code{gdbm-strerror},
in particular when there is an associated system error.
Do nothing and return @code{#f} for versions of GDBM that do
not provide the underlying C function @code{gdbm_db_strerror}.  */)
{
#define FUNC_NAME s_gg_db_strerror
  GDBM_FILE dbf;

  CHKDB ();
  SETCHK_DBF ();
#if HAVE_GDBM_RECOVER

  const char *msg = gdbm_db_strerror (dbf);

  if (! msg)
    RETURN_FALSE ();

  return STRING (msg);

#else  /* !HAVE_GDBM_RECOVER */
  RETURN_FALSE ();
#endif /* !HAVE_GDBM_RECOVER */
#undef FUNC_NAME
}

PRIMPROC
(gg_needs_recovery, "gdbm-needs-recovery?", 1, 0, 0,
 (SCM db),
 doc: /***********
Return @code{#t} if @var{db} is in an inconsistent state
and needs recovery (@pxref{Inconsistent state recovery}),
or @code{#f} for GDBM versions that do not provide
underlying C function @code{gdbm_needs_recovery}.  */)
{
#define FUNC_NAME s_gg_needs_recovery
  GDBM_FILE dbf;

  CHKDB ();
  SETCHK_DBF ();
#if HAVE_GDBM_RECOVER

  return BOOLEAN (gdbm_needs_recovery (dbf));

#else  /* !HAVE_GDBM_RECOVER */
  RETURN_FALSE ();
#endif /* !HAVE_GDBM_RECOVER */
#undef FUNC_NAME
}

PRIMPROC
(gg_happy_to_throw, "happy-to-throw", 0, 1, 0,
 (SCM setting),
 doc: /***********
Return current state of the ``happy to throw'' bit.
Optional arg @var{setting} is taken as a boolean to
set (non-@code{#f}) or clear (@code{#f}) the bit.
The return value reflects the state prior to the setting.  */)
{
#define FUNC_NAME s_gg_happy_to_throw
  SCM rv = BOOLEAN (happy_to_throw);

  if (GIVENP (setting))
    happy_to_throw = NOT_FALSEP (setting);

  return rv;
#undef FUNC_NAME
}

PRIMPROC
(gg_fdesc, "gdbm-fdesc", 1, 0, 0,
 (SCM db),
 doc: /***********
Return the file descriptor associated with @var{db},
or @code{#f} for versions of GDBM that do not
support this introspection.  */)
{
#define FUNC_NAME s_gg_fdesc
  GDBM_FILE dbf;

  CHKDB ();
  SETCHK_DBF ();
#ifndef HAVE_GDBM_FDESC
  RETURN_FALSE ();
#else /* HAVE_GDBM_FDESC */
  return NUM_INT (gdbm_fdesc (dbf));
#endif /* HAVE_GDBM_FDESC */
#undef FUNC_NAME
}

PRIMPROC
(gg_count, "gdbm-count", 1, 0, 0,
 (SCM db),
 doc: /***********
Return a count of records in @var{db},
or @code{#f} for versions of GDBM that do
not provide the underlying C function @code{gdbm_count}.  */)
{
#define FUNC_NAME s_gg_count
  GDBM_FILE dbf;

  SETCHK_DBF ();
#if HAVE_GDBM_COUNT
  {
    gdbm_count_t n;
    int ret;

    ret = gdbm_count (dbf, &n);

    RETURN_FALSE_IF_NEGATIVE (ret);
    return NUM_ULONG (n);
  }
#else  /* !HAVE_GDBM_COUNT */
  RETURN_FALSE ();
#endif  /*!HAVE_GDBM_COUNT */
#undef FUNC_NAME
}

PRIMPROC
(gg_copy_meta, "gdbm-copy-meta", 2, 0, 0,
 (SCM dst ,SCM src),
 doc: /***********
Copy metainfo (file ownership and mode) from @var{src} to @var{dst},
both db file handles.
Do nothing and return @code{#f} for GDBM versions that do not
provide the underlying C function @code{gdbm_copy_meta}.  */)
{
#define FUNC_NAME s_gg_copy_meta
  GDBM_FILE dstf, srcf;

  ASSERT (dst, DBFP (dst), 1);
  dstf = DBF (dst);
  if (! dstf)
    file_not_open_error (FUNC_NAME, dst);

  ASSERT (src, DBFP (src), 2);
  srcf = DBF (src);
  if (! srcf)
    file_not_open_error (FUNC_NAME, src);

#if HAVE_GDBM_RECOVER

  int ret;

  ret = gdbm_copy_meta (dstf, srcf);

  RETURN_FALSE_IF_NEGATIVE (ret);
  return SCM_BOOL_T;

#else  /* !HAVE_GDBM_RECOVER */
  RETURN_FALSE ();
#endif /* !HAVE_GDBM_RECOVER */
#undef FUNC_NAME
}


/* Recovery.  */

#if HAVE_GDBM_RECOVER
static void
gg_recover_notify (void *data, char const *fmt, ...)
{
  va_list args;
  char buf[128];
  SCM notify = PTR2SCM (data);
  SCM s;

  va_start (args, fmt);
  vsnprintf (buf, 128, fmt, args);
  va_end (args);

  s = STRING (buf);

  scm_call_1 (notify, s);
}
#endif  /* HAVE_GDBM_RECOVER */

PRIMPROC
(gg_recover, "gdbm-recover", 4, 2, 0,
 (SCM db, SCM max_failed_keys, SCM max_failed_buckets, SCM max_failures,
  SCM notify, SCM force),
 doc: /***********
Recover @var{db} and return values representing the status of the recovery.
2nd arg @var{max-failed-keys} is either @code{#f} or an integer that
specifies how many failed keys to process before giving up.
3rd arg @var{max-failed-buckets} is likewise for buckets.
4th arg @var{max_failures} is likewise, for any kind of failure.

Optional 5th arg @var{notify} is a procedure that takes one arg, a string.
It is called upon each recoverable or non-fatal error that occurred
during the recovery.

Normally, @code{gdbm-recover} only proceeds if there is a recovery to
be done.  Optional 6th arg @var{force} non-@code{#f} means proceed
in any case, whether or not a recovery is necessary.

Return a list of the form:

@example
(STATUS
 RECOVERED-KEYS
 RECOVERED-BUCKETS
 FAILED-KEYS
 FAILED-BUCKETS
 DUPLICATE-KEYS
 BACKUP-FILENAME)
@end example

@noindent
The first element is @code{#t} if recovery succeeded, else @code{#f}.
The next five list elements are counts (non-negative integers).
The last is a string, the name of the
backup file created at the beginning of the recovery process.

For GDBM versions that do not provide the underlying C function
@code{gdbm_recover}, do nothing and return @code{#f}.  */)
{
#define FUNC_NAME s_gg_recover
  GDBM_FILE dbf;

  SETCHK_DBF ();
#if HAVE_GDBM_RECOVER
  {
    int flags = GDBM_RCVR_BACKUP;
    gdbm_recovery rcvr = {
      .errfun = NULL,
      .data = NULL,
      .max_failed_keys = 0,
      .max_failed_buckets = 0,
      .max_failures = 0
    };
    int ret;
    SCM backup = SCM_BOOL_F;

    if (NOT_FALSEP (max_failed_keys))
      {
        SCM_VALIDATE_UINT_COPY (2, max_failed_keys, rcvr.max_failed_keys);
        flags |= GDBM_RCVR_MAX_FAILED_KEYS;
      }
    if (NOT_FALSEP (max_failed_buckets))
      {
        SCM_VALIDATE_UINT_COPY (3, max_failed_buckets, rcvr.max_failed_buckets);
        flags |= GDBM_RCVR_MAX_FAILED_BUCKETS;
      }
    if (NOT_FALSEP (max_failures))
      {
        SCM_VALIDATE_UINT_COPY (4, max_failures, rcvr.max_failures);
        flags |= GDBM_RCVR_MAX_FAILURES;
      }
    if (GIVENP (notify))
      {
        SCM_VALIDATE_PROC (5, notify);
        rcvr.data = SCM2PTR (notify);
        rcvr.errfun = gg_recover_notify;
        flags |= GDBM_RCVR_ERRFUN;
      }
    if (GIVENP (force) && NOT_FALSEP (force))
      flags |= GDBM_RCVR_FORCE;

    ret = gdbm_recover (dbf, &rcvr, flags);
    if (rcvr.backup_name)
      {
        backup = STRING (rcvr.backup_name);
        free (rcvr.backup_name);
        rcvr.backup_name = NULL;
      }

    return PCHAIN ((0 > ret
                    ? SCM_BOOL_F
                    : SCM_BOOL_T),
                   NUM_ULONG (rcvr.recovered_keys),
                   NUM_ULONG (rcvr.recovered_buckets),
                   NUM_ULONG (rcvr.failed_keys),
                   NUM_ULONG (rcvr.failed_buckets),
#if HAVE_RECOVERY_DUPLICATE_KEYS
                   NUM_ULONG (rcvr.duplicate_keys),
#else
                   SCM_INUM0,
#endif
                   backup);
  }
#else  /* !HAVE_GDBM_RECOVER */
  RETURN_FALSE ();
#endif /* !HAVE_GDBM_RECOVER */
#undef FUNC_NAME
}


/* Module magic.  */

static
void
init_module (void)
{
  int i;

  happy_to_throw = 1;

  init_symbolic_error_codes ();

  for (i = 0; i < n_mode_flags; i++)
    SYMBOLIC (mode_flags[i]) = SYMBOL (mode_flags[i].as.str);
  for (i = 0; i < n_runtime_options; i++)
    SYMBOLIC (runtime_options[i]) = SYMBOL (runtime_options[i].as.str);

  DEFSMOB (gg_db_tag, "gdbm-file",
           NULL, gdbmfile_free, gdbmfile_print);
#include "gdbm.x"
}

MOD_INIT_LINK_THUNK ("database gdbm", database_gdbm, init_module)

/* gdbm.c ends here */
