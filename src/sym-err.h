/* sym-err.h 

   Copyright (C) 2022 Thien-Thi Nguyen

   Guile-GDBM is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Guile-GDBM is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Guile-GDBM; if not, see <https://www.gnu.org/licenses/>.
*/

/**
 * Initialize the symbolic error codes module.
 */
extern void init_symbolic_error_codes (void);

/**
 * Translate symbol @var{code} to an integer (long).
 * If @var{code} is invalid, return @code{-1}.
 */
extern long enum2long (SCM code);

/**
 * Translate integer (long) @var{num} to an error code (symbol).
 * If @var{num} is invalid, return @code{#f}.
 */
extern SCM long2enum (long num);

/* sym-err.h ends here */
