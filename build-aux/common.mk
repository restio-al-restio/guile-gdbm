# common.mk
#
# Copyright (C) 2011, 2013 Thien-Thi Nguyen
#
# Guile-GDBM is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# Guile-GDBM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Guile-GDBM; if not, see <https://www.gnu.org/licenses/>.

bx = $(top_srcdir)/build-aux
gx = $(bx)/guile-baux/gbaux-do
tsopts = -c utf-8 -m '(database gdbm)'

# common.mk ends here
